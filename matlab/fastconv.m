function tmep


x    = rand(1,1500);
h    = rand(1,1500);
Ly   = length(x)+length(h)-1;
NFFT = pow2(nextpow2(Ly));
H    = fft(h, NFFT);

tic;
o1 = conv(h, x);
t1 = toc;


tic;
o2 = fastconv(x, H, NFFT);
o2 = o2(1:Ly);
t2 = toc;

close all;
plot(o1-o2);

fprintf('%0.2f\n', t2/t1);

end

function y = fastconv(x, H, NFFT)
% H is pre-calaculted fft of filter impulse

X = fft(x, NFFT);
Y = X.*H;        	       % Multiplication in freq domain is convoultion in time 
y = real(ifft(Y, NFFT));

end