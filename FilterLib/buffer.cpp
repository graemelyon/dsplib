//
//  buffer.cpp
//  FilterLib
//
//  Created by Graeme Lyon on 21/08/2016.
//  Copyright © 2016 GRAEMELYON. All rights reserved.
//

#include "buffer.hpp"

Buffer::Buffer(unsigned size) : buffersize(size), buffer_ptr(0)
{
    mybuffer = new double[buffersize];
}

Buffer::~Buffer()
{
    delete[] mybuffer;
}

unsigned Buffer::getsize()
{
    return buffersize;
}