//
//  Fir.hpp
//  FilterLib
//
//  Created by Graeme Lyon on 21/08/2016.
//  Copyright © 2016 GRAEMELYON. All rights reserved.
//

#ifndef Fir_hpp
#define Fir_hpp

class Fir {

public:
    Fir(const char *);
    ~Fir();
    double filter(double samp);
    void reset();

private:
    unsigned taps, buffer_ptr;
    double *coeffs;
    double *buffer;
    
};

#endif /* Fir_hpp */
    