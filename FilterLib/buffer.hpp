//
//  buffer.hpp
//  FilterLib
//
//  Created by Graeme Lyon on 21/08/2016.
//  Copyright © 2016 GRAEMELYON. All rights reserved.
//

#ifndef buffer_hpp
#define buffer_hpp

class Buffer {
    
public:
    Buffer(unsigned);
    ~Buffer();
    double *mybuffer;
    unsigned buffer_ptr;
    unsigned getsize();
    
private:
    unsigned buffersize;

};


#endif /* buffer_hpp */
