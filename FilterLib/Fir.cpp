//
//  FIR.cpp
//  FilterLib
//
//  Created by Graeme Lyon on 21/08/2016.
//  Copyright © 2016 GRAEMELYON. All rights reserved.
//

#include "Fir.hpp"
#include <iostream>

Fir::Fir(const char *coeffFile) : buffer_ptr(0), taps(0)
{

    // Read filter coefficients from file
    FILE *fp = fopen(coeffFile, "r");
    if (fp == NULL) {
        std::cerr << "Couldn't open coefficients file" << coeffFile << std::endl;
        exit(1);
    }

    // Count taps in file
    double a;
    while (fscanf(fp, "%lf\n", &a) != EOF) taps++;
    rewind(fp);
    
    coeffs = new double[taps];
    buffer = new double[taps]();    // initialize to zero
    
    // Read coefficients into coeff buffer
    for (int i=0; i<taps; ++i) {
        if (fscanf(fp, "%lf\n", (coeffs + i)) != 1) {
            std::cerr << "Problem reading filter ceofficients\n" << std::endl;
            exit(1);
        }
    }

}

double Fir::filter(double samp)
{
    
    buffer[buffer_ptr] = samp;
    
    double y = 0;
    for (int i=1; i<=taps; ++i) {
        y += buffer[(buffer_ptr + i) % taps]*coeffs[taps-i];
    }

    ++buffer_ptr;
    buffer_ptr %= taps;

    return y;

}

void Fir::reset()
{
    memset(buffer, 0, sizeof(double)*taps);
    buffer_ptr = 0;
}

Fir::~Fir()
{
    delete[] coeffs;
    delete[] buffer;
};