//
//  main.cpp
//  FilterLib
//
//  Created by Graeme Lyon on 21/08/2016.
//  Copyright © 2016 GRAEMELYON. All rights reserved.
//

#include <iostream>
#include "FIR.hpp"

int main(int argc, const char * argv[]) {
    
    Fir fir1("h_bp.dat");
    
    // Read input signal
    FILE *stream = fopen("myECG_Ch2.dat", "r");
    if (stream == NULL) {
        printf("Error opening ECG file\n");
        exit(1);
    }
    // Open output stream
    FILE *fp_out = fopen("filtered.dat", "w");
    if (fp_out == NULL) {
        printf("Error opening output file\n");
        exit(1);
    }
    // Read input stream and filter signal
    double samp;
    while (fscanf(stream, "%lf\r", &samp) != EOF) {
        
        // Filter
        double y = fir1.filter(samp);
        fprintf(fp_out, "%lf\n", y);
        
    }
    fclose(stream);
    fclose(fp_out);
    
    std::cout << "Complete" << std::endl;
    
    return 0;

}
