set(0,'DefaultFigureWindowStyle','docked')
clear;
% close all;
 
load('sig');

fs = 1000;
b = fir1(50, [40 60]./(fs/2), 'stop');
z = filter(b, 1, x);


myC = importdata('filtered.dat');


figure; hold on;
plot(z,'s-k', 'markersize',8);
plot(myC,'o-r');
legend('filter', 'myC');
% linkaxes(ax,'x');
% xlim(1e4 *[4.1546    4.1651]);

fid = fopen('h_bp.dat','w'); for i=1:length(b), fprintf(fid, '%0.16f\n', b(i)); end; fclose(fid);